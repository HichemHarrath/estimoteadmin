//
//  AppDelegate.h
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 09/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
