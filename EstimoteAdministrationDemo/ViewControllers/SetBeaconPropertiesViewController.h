//
//  SetBeaconPropertiesViewController.h
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 10/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ESTBeacon.h"

#import "BeaconHTTPClient.h"

@interface SetBeaconPropertiesViewController : UIViewController <UIPopoverControllerDelegate, UIAlertViewDelegate, ESTBeaconDelegate, BeaconHTTPClientDelegate>

@property (nonatomic, retain) ESTBeacon * beacon;
@property (strong, nonatomic) IBOutlet UIView *estimoteBeaconBackView;

@property (strong, nonatomic) IBOutlet UIImageView *estimoteBeaconImageView;

- (IBAction)connectToBeaconBtnPressed:(id)sender;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *connectionActivityIndicator;
@property (strong, nonatomic) IBOutlet UILabel *beaconInfoLabel;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *chooseBtn;
- (IBAction)chooseBtnPressed:(id)sender;

@property (strong, nonatomic) IBOutlet UITextView *helpTextView;

- (IBAction) saveBeaconBtnPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *beaconImageView;
@property (strong, nonatomic) IBOutlet UIButton *saveBtn;


@property (strong, nonatomic) IBOutlet UITextField *beaconNameTextField;


@property (nonatomic, retain) NSString * imageName;


@end
