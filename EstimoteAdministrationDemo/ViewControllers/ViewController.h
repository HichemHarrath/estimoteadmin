//
//  ViewController.h
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 09/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESTBeaconManager.h"

@interface ViewController : UIViewController <ESTBeaconManagerDelegate>


@property (strong, nonatomic) IBOutlet UIImageView *iPadIconImageView;


- (IBAction)signOutBtnPressed:(id)sender;


- (IBAction)startScanningForBeaconsBtnPressed:(id)sender;

@end
