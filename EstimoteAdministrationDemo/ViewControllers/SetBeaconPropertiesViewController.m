//
//  SetBeaconPropertiesViewController.m
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 10/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import "SetBeaconPropertiesViewController.h"
#import "PulsingHaloLayer.h"

#import "EstimotePrametersTableViewController.h"
#import "PubCollectionViewController.h"
#import "BeaconColorPopoverViewController.h"

#import "ChoosePubPopoverViewController.h"

#import "BeaconHTTPClient.h"
@interface SetBeaconPropertiesViewController ()


@property (nonatomic, strong) UIPopoverController * colorPickerPopover;
@property (nonatomic, retain) BeaconColorPopoverViewController * beaconColorPopoverViewController ;

@property (nonatomic, strong) PulsingHaloLayer * pulse;

@property (nonatomic, retain) EstimotePrametersTableViewController * parametersTableViewController ;

@property (nonatomic, retain) NSString * broadcastingPowerStr;
@property (nonatomic, assign) int adIntervalValue;;



@property (nonatomic, retain) UIPopoverController* choosePubPopover ;
@property (nonatomic, retain) ChoosePubPopoverViewController * pubpopoverContentViewController;

@property (nonatomic, assign) BOOL didUserSelectColor ;

@property (nonatomic, retain) NSString * beaconImageName;

@property (nonatomic, retain) NSMutableDictionary * paramertersDict;


@property (nonatomic, retain) UIAlertView * disconnectionAlert ;


@end

@implementation SetBeaconPropertiesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void) setPulseAnnimationValues {
    
    NSLog(@"notification value == %@", self.broadcastingPowerStr);
    
    if ([[self.estimoteBeaconBackView.layer sublayers] containsObject:self.pulse]){
        [self.pulse removeFromSuperlayer];
        self.pulse = [PulsingHaloLayer layer];
        self.pulse.position = self.estimoteBeaconImageView.center;

    }

    
    if ([self.broadcastingPowerStr rangeOfString:@"Strong"].location != NSNotFound) {
        
        // Strong signal
        self.pulse.backgroundColor = [UIColor redColor].CGColor;
        self.pulse.radius = 200.0;
        
    } else if ([self.broadcastingPowerStr rangeOfString:@"Normal"].location != NSNotFound) {
        
        //normal signal
        self.pulse.backgroundColor = [UIColor orangeColor].CGColor;
        self.pulse.radius = 150.0;
        
        
        
    } else if ([self.broadcastingPowerStr rangeOfString:@"Weak"].location != NSNotFound) {
        
        //weak signal
        self.pulse.backgroundColor = [UIColor greenColor].CGColor;
        self.pulse.radius = 110.0;
        
        
    } else {
        
        // could not get signal power
        UIColor *color = [UIColor colorWithRed:(3/255.0) green:(228/255.0) blue:(245/255.0) alpha:1.0];
        self.pulse.backgroundColor = color.CGColor;
        
        self.pulse.radius = 150.0 ;
        
    }
    
    

    

    self.pulse.animationDuration = self.adIntervalValue/1000 ;
    float ff = ((float)self.adIntervalValue)/1000;
    NSLog(@"self.adIntervalValue  ==== %d ",self.adIntervalValue);
    NSLog(@" self.pulse.animationDuration ==  %f", ff);
    
    
    // add pulse annimation
    
    

    [self.estimoteBeaconBackView.layer insertSublayer:self.pulse below:self.estimoteBeaconImageView.layer];
    
    [self.parametersTableViewController.tableView reloadData];


}



-(NSString *) getBroadcastingPowerString:(ESTBeaconPower) value {
    
    NSString * resultStr = @"";
    
    
    switch (value) {
        case ESTBeaconPowerLevel8:{
            resultStr = @"Strong (4 dBm)";
            break;
        }
        case ESTBeaconPowerLevel7:{
            resultStr = @"Strong (0 dBm)";
            break;
        }
        case ESTBeaconPowerLevel6:{
            resultStr = @"Normal (-4 dBm)";
            break;
        }
        case ESTBeaconPowerLevel5:{
            resultStr = @"Normal (-8 dBm)";
            break;
        }
        case ESTBeaconPowerLevel4:{
            resultStr = @"Weak (-12 dBm)";
            break;
        }
        case ESTBeaconPowerLevel3:{
            resultStr = @"Weak (-16 dBm)";
            break;
        }
        case ESTBeaconPowerLevel2:{
            resultStr = @"Weak (-20 dBm)";
            break;
        }
        case ESTBeaconPowerLevel1:{
            resultStr = @"Weak (-30 dBm)";
            break;
        }


            
        default:
            break;
    }
    
    return resultStr;

}
-(void) IDChangedSucessNotificationReceived:(NSNotification *) notification {
    
    
    NSLog(@"IDChangedSucessNotificationReceived");
    [self establishConnectionTobeacon];


    NSMutableDictionary * notifDict = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary *) notification.object];
    
    if ([notifDict objectForKey:@"newPowerValue"]) {
        
        int newPowerValue = [((NSNumber *) [notifDict objectForKey:@"newPowerValue"]) intValue];
        
        NSLog(@"new powerValue = %d",newPowerValue);
        self.broadcastingPowerStr = [self getBroadcastingPowerString:newPowerValue] ;
        self.parametersTableViewController.broadcastingPowerLabel.text = self.broadcastingPowerStr;
        
    } else if ([notifDict objectForKey:@"newAdInterval"] ) {
        
        int newAdInterval = [((NSNumber *)[notifDict objectForKey:@"newAdInterval"]) intValue];
        self.adIntervalValue = newAdInterval ;
        [self changePulseAnimationDuration:newAdInterval];
        
        self.parametersTableViewController.advertisingIntervalLabel.text = [NSString stringWithFormat:@"%d ms",newAdInterval];
        [self.parametersTableViewController.tableView reloadData];
        
        
    }
    [self setPulseAnnimationValues];

    
    
}

-(void) changePulseAnimationDuration:(int) newAdInterval {

}

-(void) IDChangedFailedNotificationReceived {
    
    NSLog(@"IDChangedFailedNotificationReceived");
    [self establishConnectionTobeacon];
    [self setBeaconProperties];

    

    
}

-(void) UserDidChangeColorNotificationRecieved:(NSNotification *) notif {
    
    NSLog(@"UserDidChangeColorNotification");
    self.beaconImageName =(NSString *)notif.object ;
    
    [self.beaconImageView setImage:[UIImage imageNamed:((NSString *)notif.object)]];
    self.didUserSelectColor  = YES;
    
    NSString *  imageNameKey = [NSString stringWithFormat:@"%@.%ld.%ld", [self.beacon.proximityUUID UUIDString], (long)[self.beacon.major integerValue], (long)[self.beacon.minor integerValue]];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:imageNameKey]) {
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:imageNameKey] rangeOfString:@"estimote_beacon"].location != NSNotFound) {
            
            self.parametersTableViewController.colorLabel.text = @"Icy Marshmallow" ;
        } else if ([[[NSUserDefaults standardUserDefaults] objectForKey:imageNameKey] rangeOfString:@"BlueberryPieEstimoteIcon"].location != NSNotFound){
            
            self.parametersTableViewController.colorLabel.text = @"Blueberry Pie" ;
        }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:imageNameKey] rangeOfString:@"CoctailMintEstimoteIcon"].location != NSNotFound){
            
            self.parametersTableViewController.colorLabel.text = @"Coctail Mint";
            
        }
    }

    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(IDChangedSucessNotificationReceived:)
                                                 name:@"IDChangedSucessNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(IDChangedFailedNotificationReceived)
                                                 name:@"IDChangedFailedNotification" object:nil];
    

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(UserDidChangeColorNotificationRecieved:)
                                                 name:@"UserDidChangeColorNotification"
                                               object:nil];
    self.beacon.delegate = self;
    
    self.pulse = [PulsingHaloLayer layer];
    self.pulse.position = self.estimoteBeaconImageView.center;
    
    
    self.pulse.animationDuration = 0.7;
    self.connectionActivityIndicator.hidden = YES;
    
    
    
    [self establishConnectionTobeacon];



    //self.chooseBtn.enabled = NO ;
    self.didUserSelectColor = NO;
    
    if (self.imageName) {
        
        self.beaconImageView.image = [UIImage imageNamed:self.imageName];

    } else {
        
        self.beaconImageView.image = [UIImage imageNamed:@"estimote_beacon.png"];

    }
    


}
-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    [self.beacon disconnectBeacon];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (IBAction)connectToBeaconBtnPressed:(id)sender {
    
    [self establishConnectionTobeacon];

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"embeddedSegue"]) {
        
        [((EstimotePrametersTableViewController *) segue.destinationViewController).tableView reloadData];

        self.parametersTableViewController = (EstimotePrametersTableViewController *) segue.destinationViewController;
        self.parametersTableViewController.beacon = self.beacon ; 
    }

}

-(void) establishConnectionTobeacon {
    
    if (![self.beacon isConnected]) {
        
        //connect to Selected beacon
        self.connectionActivityIndicator.hidden = NO;
        [self.connectionActivityIndicator startAnimating];
        self.beaconInfoLabel.text = @"Connecting ..." ;
        
        [self.beacon connectToBeacon];

    }
    
    


}



- (IBAction)chooseBtnPressed:(id)sender {
    
//    if (!self.pubpopoverContentViewController) {
//        
//        UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        self.pubpopoverContentViewController = (ChoosePubPopoverViewController *)[storyBoard instantiateViewControllerWithIdentifier:@"ChoosePubWebViewID"];
//        
//        
//    }
//    
//    if (!self.choosePubPopover) {
//        
//        self.choosePubPopover = [[UIPopoverController alloc] initWithContentViewController:self.pubpopoverContentViewController];
//        self.choosePubPopover.delegate = self;
//    }
//    
//    [self.choosePubPopover presentPopoverFromBarButtonItem:self.chooseBtn
//                                  permittedArrowDirections:UIPopoverArrowDirectionUp
//                                                  animated:YES];
    
}

-(void)setBeaconProperties {
    self.broadcastingPowerStr = @"";
    
    NSLog(@"Connection sucess :)");
    [self.connectionActivityIndicator stopAnimating];
    self.connectionActivityIndicator.hidden = YES ;
    
    [self.beaconInfoLabel setText:@"Connected"];
    
    
    [self.beacon readBeaconProximityUUIDWithCompletion:^(NSString *value, NSError *error) {
        //self.UUIDLabel.text = value;
        
        self.parametersTableViewController.UUIDLabel.text = value;
        
        if (error) {
            self.parametersTableViewController.UUIDLabel.text = @"ERROR" ;
        }
    }];
    
    [self.beacon readBeaconMajorWithCompletion:^(unsigned short value, NSError *error) {
        // self.majorLabel.text = [NSString stringWithFormat:@"%hu",value];
        
        self.parametersTableViewController.majorLabel.text = [NSString stringWithFormat:@"%hu",value];
        
        if (error) {
            self.parametersTableViewController.majorLabel.text = @"ERROR" ;
        }
        
    }];
    
    [self.beacon readBeaconMinorWithCompletion:^(unsigned short value, NSError *error) {
        // self.MinorLabel.text = [NSString stringWithFormat:@"%hu",value];
        self.parametersTableViewController.minorLabel.text = [NSString stringWithFormat:@"%hu",value];
        
        if (error) {
            self.parametersTableViewController.minorLabel.text = @"ERROR" ;
        }
        
    }];
    
    [self.beacon readBeaconHardwareVersionWithCompletion:^(NSString *value, NSError *error) {
        
        self.parametersTableViewController.hardwareRevisionLabel.text = value ;
        
        if (error) {
            self.parametersTableViewController.hardwareRevisionLabel.text = @"ERROR" ;
        }
        
    }];
    
    
    [self.beacon readBeaconFirmwareVersionWithCompletion:^(NSString *value, NSError *error) {
        
        self.parametersTableViewController.operatingSystemLabel.text = [NSString stringWithFormat:@"Estimote OS %@",value] ;
        
        
        if (error) {
            self.parametersTableViewController.operatingSystemLabel.text = @"ERROR" ;
        }
        
    }];
    
    
    [self.beacon readBeaconBatteryWithCompletion:^(unsigned short value, NSError *error) {
        
        self.parametersTableViewController.batteryLevelLabel.text = [NSString stringWithFormat:@"%d %%",value];
        
        if (error) {
            self.parametersTableViewController.batteryLevelLabel.text = @"ERROR" ;
        }
        
    }];
    
    [self.beacon readBeaconPowerWithCompletion:^(ESTBeaconPower value, NSError *error) {
        
        self.broadcastingPowerStr = [self getBroadcastingPowerString:value] ;
        self.parametersTableViewController.broadcastingPowerLabel.text = self.broadcastingPowerStr ;
        
        [self setPulseAnnimationValues];
        [self.parametersTableViewController.tableView reloadData];
        
        
        if (error) {
            
            self.broadcastingPowerStr = @"error" ;
            self.parametersTableViewController.broadcastingPowerLabel.text = @"ERROR" ;
            [self.parametersTableViewController.tableView reloadData];
            
        }
        
    }];
    
    [self.beacon readBeaconAdvIntervalWithCompletion:^(unsigned short value, NSError *error) {
        
        self.parametersTableViewController.advertisingIntervalLabel.text = [NSString stringWithFormat:@"%d ms",value];
        
        [self.parametersTableViewController.tableView reloadData];
        self.adIntervalValue = value ;
        [self setPulseAnnimationValues];
        
        if (error) {
            
            self.parametersTableViewController.advertisingIntervalLabel.text = @"ERROR" ;
            [self.parametersTableViewController.tableView reloadData];
            
        }
        
    }];
    
    NSString *  imageNameKey = [NSString stringWithFormat:@"%@.%ld.%ld", [self.beacon.proximityUUID UUIDString], (long)[self.beacon.major integerValue], (long)[self.beacon.minor integerValue]];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:imageNameKey]) {
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:imageNameKey] rangeOfString:@"estimote_beacon"].location != NSNotFound) {
            
            self.parametersTableViewController.colorLabel.text = @"Icy Marshmallow" ;
        } else if ([[[NSUserDefaults standardUserDefaults] objectForKey:imageNameKey] rangeOfString:@"BlueberryPieEstimoteIcon"].location != NSNotFound){
            
            self.parametersTableViewController.colorLabel.text = @"Blueberry Pie" ;
        }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:imageNameKey] rangeOfString:@"CoctailMintEstimoteIcon"].location != NSNotFound){
            
            self.parametersTableViewController.colorLabel.text = @"Coctail Mint";
            
        }
    }

    
    [self.parametersTableViewController.tableView reloadData];

}

#pragma mark Beacon Delegate
- (void) beaconConnectionDidSucceeded:(ESTBeacon *)beacon {
    
    [self setBeaconProperties];
    
}

-(void) beaconConnectionDidFail:(ESTBeacon *)beacon withError:(NSError *)error {
    
    self.disconnectionAlert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                                  message:@"Connection To Beacon Failed, Please try to connect again."
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                        otherButtonTitles:@"Reconnect",
                                        nil];
    self.disconnectionAlert.delegate = self;

    [self.disconnectionAlert show];
    
}

-(void) beaconDidDisconnect:(ESTBeacon *)beacon withError:(NSError *)error {
    
    self.disconnectionAlert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                                  message:@"Connection with Beacon was lost, Do you want to reconnect?"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                        otherButtonTitles:@"Reconnect",
                                        nil];
    
    self.disconnectionAlert.delegate = self;
    [self.disconnectionAlert show];

    
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
   
    if (self.disconnectionAlert == alertView) {
        
        if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Reconnect"]) {
            
            [self establishConnectionTobeacon];
        }
    }
  
}

- (IBAction)saveBeaconBtnPressed:(id)sender {
    
    NSLog(@"self.beaconNameTextField.text == %@",self.beaconNameTextField.text);
    
    if (!self.beaconNameTextField.text || [self.beaconNameTextField.text isEqualToString:@""] ) {
        
        [self showAlertWithMessage:@"Please choose name for your beacon" andTitle:@"Error"];
        return;
    }
    
    self.paramertersDict = [NSMutableDictionary dictionary];
    
    [self.beacon readBeaconProximityUUIDWithCompletion:^(NSString *value, NSError *error) {
        
        
        if (error) {
            [self showAlertWithMessage:@"Failed to read UUID from your Beacon. Please try again." andTitle:@"Error"];
            return ;
        }
        [self.paramertersDict setObject:value forKey:@"uuid"] ;

        
        [self.beacon readBeaconMajorWithCompletion:^(unsigned short value, NSError *error) {
            
            
            if (error) {
                [self showAlertWithMessage:@"Failed to read Major from your Beacon. Please try again." andTitle:@"Error"];
                return ;
            }
            
            [self.paramertersDict setObject:[NSString stringWithFormat:@"%hu",value] forKey:@"major"] ;

            
            [self.beacon readBeaconMinorWithCompletion:^(unsigned short value, NSError *error) {
                
                if (error) {
                    
                    [self showAlertWithMessage:@"Failed to read Minor from your Beacon. Please try again." andTitle:@"Error"];
                    return ;
                }
                
                
                
                [self.paramertersDict setObject:[NSString stringWithFormat:@"%hu",value] forKey:@"minor"];
                
              
                [self.paramertersDict setObject:@"123456" forKey:@"key"];
                [self.paramertersDict setObject:self.beaconNameTextField.text forKey:@"name"];
                
                [self presentColorsPickerPopover];
                
                

                
                // here parameters Dict is completed
                
                
            }];
            
        }];
        
    }];
}

-(void) showAlertWithMessage : (NSString *) msg andTitle:(NSString *) title {
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                                  message:msg
                                                                 delegate:self
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles: nil];
    [alertView show];


}

-(NSString *) getColorForSelectedBeacon {
    
    NSString * result = @"" ;
    
    if (!self.didUserSelectColor) {
        
        
        if ([self.beaconColorPopoverViewController.selectedColorImageName isEqualToString:@"estimote_beacon.png"]) {
            //Icy Marshmallow
            
            result = @"6dc8ed";
            self.beaconImageView.image = [UIImage imageNamed:@"estimote_beacon.png"];
            
        } else if ([self.beaconColorPopoverViewController.selectedColorImageName isEqualToString:@"BlueberryPieEstimoteIcon.png"]) {
            //Blueberry Pie
            
            result = @"605097";
            self.beaconImageView.image = [UIImage imageNamed:@"BlueberryPieEstimoteIcon.png"];

            
        }else if ([self.beaconColorPopoverViewController.selectedColorImageName isEqualToString:@"CoctailMintEstimoteIcon.png"]){
            //Coctail Mint
            
            result = @"abdcc9";
            self.beaconImageView.image = [UIImage imageNamed:@"CoctailMintEstimoteIcon.png"];

            
        }
        
    } else {
        
        
        if ([self.beaconImageName isEqualToString:@"estimote_beacon.png"]) {
            
            //Icy Marshmallow
            result = @"6dc8ed";
            self.beaconImageView.image = [UIImage imageNamed:@"estimote_beacon.png"];
            
        } else if ([self.beaconImageName isEqualToString:@"BlueberryPieEstimoteIcon.png"]) {
            
            //Blueberry Pie
            result = @"605097";
            self.beaconImageView.image = [UIImage imageNamed:@"BlueberryPieEstimoteIcon.png"];

            
        }else if ([self.beaconImageName isEqualToString:@"CoctailMintEstimoteIcon.png"]){
            
            //Coctail Mint
            result = @"abdcc9";
            self.beaconImageView.image = [UIImage imageNamed:@"CoctailMintEstimoteIcon.png"];

            
        }
        
        
    }

    return result;
}



-(void) presentColorsPickerPopover {

    
    if (!self.beaconColorPopoverViewController) {
        
        UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.beaconColorPopoverViewController = (BeaconColorPopoverViewController *) [storyBoard instantiateViewControllerWithIdentifier:@"ColorPicker"];
        self.beaconColorPopoverViewController.beacon = self.beacon ; 
    }
    
    if (!self.colorPickerPopover) {
        
        self.colorPickerPopover = [[UIPopoverController alloc] initWithContentViewController:self.beaconColorPopoverViewController];
        
        self.colorPickerPopover.delegate = self ;
    }
    
    [self.colorPickerPopover presentPopoverFromRect:self.saveBtn.frame
                                             inView:self.view
                           permittedArrowDirections:UIPopoverArrowDirectionDown
                                           animated:YES];
    
    
    
}

-(void) popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    
 if (popoverController == self.colorPickerPopover) {
        
     NSString * str  = @"";
     if ([[self getColorForSelectedBeacon] isEqualToString:@""]) {
         
         str = @"6dc8ed"  ;
         
     } else {
         
     str = [self getColorForSelectedBeacon]  ;
         
    }
       [self.paramertersDict setObject:str forKey:@"hexa"];
     
     [BeaconHTTPClient sharedBeaconHTTPClient].delegate = self;
     [[BeaconHTTPClient sharedBeaconHTTPClient] saveBeaconWithParameters:self.paramertersDict];

     self.colorPickerPopover = nil;
     self.beaconColorPopoverViewController = nil;
     
    }
    
}


-(void) beaconHTTPClient:(BeaconHTTPClient *)client didSaveBeaconWithSuccess:(id)response {
    
    NSLog(@"didSaveBeaconWithSuccess");
    
    if ([((NSDictionary *) response) objectForKey:@"created"]) {
        
        [self showAlertWithMessage:@"Your Beacon was saved with success, you can now choose an action for your beacon" andTitle:@"Congratulations"];
        self.chooseBtn.enabled = YES;
        
        
    }


}
-(void) beaconHTTPClient:(BeaconHTTPClient *)client didFailSavingBeaconWithError:(NSError *)error {
    
    NSLog(@"didFailSavingBeaconWithError");
    [self showAlertWithMessage:[error description] andTitle:@"Error"];

}


@end
