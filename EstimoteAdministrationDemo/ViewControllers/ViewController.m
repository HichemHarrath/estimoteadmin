//
//  ViewController.m
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 09/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//


#import "ESTBeaconManager.h"
#import "ESTBeaconRegion.h"



#import "ViewController.h"
#import "PulsingHaloLayer.h"
#import "CRToast.h"


#import "SetBeaconPropertiesViewController.h"




#define IBEACONS_UUID @"B9407F30-F5F8-466E-AFF9-25556B57FE6D"
#define FIRST_ESTIMOTE_BEACON_MINOR 44662
#define SECOND_ESTIMOTE_BEACON_MINOR 44663


@interface ViewController ()

@property (nonatomic, strong) PulsingHaloLayer * pulse;
@property (nonatomic, retain) NSDictionary * notificationViewOptions;

@property (nonatomic, assign) BOOL scannigForBeaconsBtnIsSelected ;


@property (nonatomic, strong) ESTBeaconManager * iBeaconsManager ;
@property (nonatomic, strong) ESTBeaconRegion * iBeaconsRegion ;



@property (nonatomic, retain) NSMutableArray * beaconsArray ;

@property (nonatomic, retain) NSString * beaconImageName ;


@end

@implementation ViewController

@synthesize scannigForBeaconsBtnIsSelected = _scannigForBeaconsBtnIsSelected ;
@synthesize notificationViewOptions = _notificationViewOptions;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.pulse = [PulsingHaloLayer layer];
    self.pulse.position = self.iPadIconImageView.center;
    
    
    UIColor *color = [UIColor colorWithRed:(3/255.0) green:(228/255.0) blue:(245/255.0) alpha:1.0];
    self.pulse.backgroundColor = color.CGColor;
    self.pulse.animationDuration = 1.3;
    
    self.pulse.radius = 600.0;
    
    _scannigForBeaconsBtnIsSelected = NO ;
    
    //init ibeacons manager and region
    [self initBeaconManagerAndRegion];
    
}


-(void) showNotificationView :(UIBarButtonItem *) sender{
    
    sender.enabled = NO;
    [CRToastManager showNotificationWithOptions:_notificationViewOptions
                                completionBlock:^(void) {
                                    NSLog(@"Completed");
                                    sender.enabled = YES;
                                    
                                }];

}

- (IBAction)signOutBtnPressed:(id)sender {
    
    [ self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)startScanningForBeaconsBtnPressed:(id)sender {
    
    
    UIColor * notificationViewBackgroundColor = [UIColor colorWithRed:(52/255.0) green:(170/255.0) blue:(220/255.0) alpha:1.0] ;
    
    if (!_scannigForBeaconsBtnIsSelected) {
        
        // DEVICE START SCANNIG FOR BEACONS
        [self.view.layer insertSublayer:self.pulse below:self.iPadIconImageView.layer];
        _scannigForBeaconsBtnIsSelected = YES ;
        
        //show notification view when scan btn pressed
        _notificationViewOptions = [self settingCRToastOptionsWithText:@"Start scannig for beacons. To configure your beacon, please put it on the iPad than tap on the nearest beacon to the Ipad icon." andBackgroundColor:notificationViewBackgroundColor andTimeInterval:10.0];
        [self showNotificationView:(UIBarButtonItem *)sender ];
        
        //Start monitoring & rangig for beacons
        [self startScanningForBeacons];
        

    } else {
        
        // DEVICE STOP SCANNIG FOR BEACONS
        [self.pulse removeFromSuperlayer];
        
        _scannigForBeaconsBtnIsSelected = NO ;
        
        //show notification view when scan btn pressed
        _notificationViewOptions = [self settingCRToastOptionsWithText:@"Stop scannig for beacons"
                                                    andBackgroundColor:notificationViewBackgroundColor
                                                       andTimeInterval:3.0];
        [self showNotificationView:(UIBarButtonItem *)sender ];
        
        //Stop monitoring & rangig for beacons
        [self stopScanningForBeacons];

    
    }
    
}

-(NSDictionary *) settingCRToastOptionsWithText :(NSString *) CRToastText andBackgroundColor:(UIColor *) backgroundColor andTimeInterval:(float)timeInterval{
    
    NSDictionary *options = @{
                              kCRToastNotificationTypeKey :@(CRToastTypeNavigationBar),
                              kCRToastTextKey : CRToastText,
                              kCRToastFontKey : [UIFont boldSystemFontOfSize:15.0],
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastBackgroundColorKey : backgroundColor,
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeSpring),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionBottom),
                              kCRToastTimeIntervalKey :  @(timeInterval),
                              kCRToastAnimationInTimeIntervalKey : @0.3,
                              kCRToastAnimationOutTimeIntervalKey : @0.3,
                              };
    return options;
}


- (void) beaconBtnPressed:(id) sender {
    //HERE push The next View controller
    
    [self performSegueWithIdentifier:@"beaconPressed" sender:sender];

}



-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"beaconPressed"]) {
        
        
        ESTBeacon * beacon =[self.beaconsArray objectAtIndex:((UIButton *)sender).tag];
        ((SetBeaconPropertiesViewController *)segue.destinationViewController).beacon = beacon;
        
        NSString * resultStr = @"";
        NSString * imageNameKey = @"";
        
        
        imageNameKey = [NSString stringWithFormat:@"%@.%ld.%ld", [beacon.proximityUUID UUIDString], (long)[beacon.major integerValue], (long)[beacon.minor integerValue]];
        
        NSLog(@"image name key == %@",imageNameKey);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:imageNameKey]) {
            
            resultStr = [[NSUserDefaults standardUserDefaults] objectForKey:imageNameKey] ;
            
        } else {
            
            resultStr = @"estimote_beacon.png" ;
        }

        
        ((SetBeaconPropertiesViewController *)segue.destinationViewController).imageName = resultStr ;
    }
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - iBeacons methods

-(void) initBeaconManagerAndRegion {
    
    self.iBeaconsManager = [[ESTBeaconManager alloc] init];
    self.iBeaconsManager.delegate = self;
    
    NSUUID * ibeaconsUUID = [[NSUUID alloc] initWithUUIDString:IBEACONS_UUID];
    
    self.iBeaconsRegion = [[ESTBeaconRegion alloc] initWithProximityUUID:ibeaconsUUID identifier:@"ESTIMOTE_BEACONS"];
    
}

- (void) startScanningForBeacons{
    
    //Start monitoring & rangig for beacons
    [self.iBeaconsManager startMonitoringForRegion:self.iBeaconsRegion];
    [self.iBeaconsManager startRangingBeaconsInRegion:self.iBeaconsRegion];

}

- (void) stopScanningForBeacons{
    
    //Stop monitoring & rangig for beacons
    [self.iBeaconsManager stopMonitoringForRegion:self.iBeaconsRegion];
    [self.iBeaconsManager stopRangingBeaconsInRegion:self.iBeaconsRegion];

    
}

-(void) postLocalNotificationWithMessage:(NSString *) msg{
    
    UILocalNotification *notification = [UILocalNotification new];
    notification.alertBody = msg;
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    
    
}
- (void) createButtonForBeacon :(ESTBeacon *) beacon {
    

    UIButton * beaconButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [beaconButton setFrame:CGRectMake(100, 100, 28, 43)];
    
    NSString * estimoteImageName = [self getSavedEstimoteImageName:beacon];
    
    [beaconButton setImage:[UIImage imageNamed:estimoteImageName] forState:UIControlStateNormal];
    
    float newYPos = 370 + ((float)beacon.rssi / -100.) * (self.view.bounds.size.height - 540);
    beaconButton.center = CGPointMake(self.view.bounds.size.width / 2, newYPos);
    
    beaconButton.tag = [self.beaconsArray indexOfObject:beacon];
    
    [beaconButton addTarget:self action:@selector(beaconBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:beaconButton];
    
    
}

-(void) removeAllBeaconsButtons {
    
    NSArray * allSubviewsArray = [self.view subviews];
    
    for (int i = 0 ; i < [allSubviewsArray count] ; i++) {
        
        if ([[allSubviewsArray objectAtIndex:i] isKindOfClass:[UIButton class]]) {
            
            [[allSubviewsArray objectAtIndex:i] removeFromSuperview];
        }
    }
}

-(NSString *) getSavedEstimoteImageName:(ESTBeacon *) beacon {
    
    NSString* resultStr = @"";
    
    NSString * imageNameKey = @"";
    
    
    imageNameKey = [NSString stringWithFormat:@"%@.%ld.%ld", [beacon.proximityUUID UUIDString], (long)[beacon.major integerValue], (long)[beacon.minor integerValue]];
    
    NSLog(@"image name key == %@",imageNameKey);
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:imageNameKey]) {
        
        resultStr = [[NSUserDefaults standardUserDefaults] objectForKey:imageNameKey] ;
        
    } else {
        
        resultStr = @"estimote_beacon.png" ;
    }
    
    self.beaconImageName = resultStr ;
    return resultStr;

}



#pragma mark - iBeacons manager delegate

-(void) beaconManager:(ESTBeaconManager *)manager didEnterRegion:(ESTBeaconRegion *)region {
    
    
    if ([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive) {
       // [self postLocalNotificationWithMessage:@"You did enter beacon region" ];

    }
    
    _notificationViewOptions = [self settingCRToastOptionsWithText:@"You did enter beacon region"
                                                andBackgroundColor:[UIColor greenColor]
                                                   andTimeInterval:4.0];
    [self showNotificationView:nil];
    
    

}

-(void) beaconManager:(ESTBeaconManager *)manager didExitRegion:(ESTBeaconRegion *)region {
    
    if ([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive) {
      //  [self postLocalNotificationWithMessage:@"You did exit beacon region" ];

    }

    _notificationViewOptions = [self settingCRToastOptionsWithText:@"You did exit beacon region"
                                                andBackgroundColor:[UIColor redColor]
                                                   andTimeInterval:4.0];
    [self showNotificationView:nil];


}

-(void) beaconManager:(ESTBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region {
    
    if ([beacons count] > 0) {
        
        NSLog(@"Ranged beacons count == %lu",(unsigned long)[beacons count]);
        self.beaconsArray = [NSMutableArray arrayWithArray:beacons] ;
        
        [self removeAllBeaconsButtons];
        
        for (ESTBeacon * beacon in beacons) {
            
            [self createButtonForBeacon:beacon];
        }
        

    }

}

-(void) beaconManager:(ESTBeaconManager *)manager didDiscoverBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region {
    
    NSLog(@"didDiscoverBeacons");

}


// unwind segue
- (IBAction) unwindSegue:(UIStoryboardSegue *) segue {

}
@end
