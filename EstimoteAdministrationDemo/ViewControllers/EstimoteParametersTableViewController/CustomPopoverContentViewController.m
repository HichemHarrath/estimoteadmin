//
//  CustomPopoverContentViewController.m
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 11/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import "CustomPopoverContentViewController.h"

@interface CustomPopoverContentViewController ()

@end

@implementation CustomPopoverContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.slider.enabled = YES;

    if ([self.popoverKey isEqualToString:@"BRAODCASTING_POWER"]) {
        
        
        self.toolBarTitle.text = @"Broadcastring Power";
        
        self.helpTextView.text = @"Here you can adjust broadcasting power"
        @"\n \n Note that broadcasting power will improve range but may negatively impact battery life." ;
        
        
        self.sliderMaximumLabel.text = @"High";
        self.sliderMinimumLabel.text = @"Low";
        
        self.firstFieldTitleLabel.text = @"Current power: ";
        self.secondFieldTitleLabel.text = @"Maximum range: ";
        
        
        self.slider.maximumValue = 4.0 ;
        self.slider.minimumValue = -30.0 ;

        [self.beacon readBeaconPowerWithCompletion:^(ESTBeaconPower value, NSError *error) {
            
            self.slider.value = value ;
            [self setBroadcastingValue:value];
            
        }];
        
        NSLog(@"self.sliderCurrentValue == %f",self.sliderCurrentValue);
        

        
    } else if ([self.popoverKey isEqualToString:@"ADVERTISING_INTERVAL"]) {
        
        
        self.toolBarTitle.text = @"Advertising Interval";
        
        self.helpTextView.text = @"Here you can adjust the advertising interval"
        @"\n \n Note that short advertising interval improve precision and responsivness but may negatively impact battery life." ;
        
        self.sliderMaximumLabel.text = @"Short";
        self.sliderMinimumLabel.text = @"Long";
        
        self.firstFieldTitleLabel.text = @"Current interval: ";
        self.secondFieldTitleLabel.text = @"Minimum value : ";
        self.secondFieldValueLabel.text = @"50 ms";
        
        self.slider.maximumValue = 1950 ;
        self.slider.minimumValue = 0 ;
        
        self.slider.continuous = YES;

        [self.beacon readBeaconAdvIntervalWithCompletion:^(unsigned short value, NSError *error) {
            
            self.slider.value = (-1)  * value + 2000;
            
            NSLog(@"slider value == %hu",value);
            self.firstFieldValueLabel.text = [NSString stringWithFormat:@"%hu ms",value];
        }];
        

    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)changeBtnPressed:(id)sender {
    self.slider.enabled = YES;
}

- (IBAction)sliderAction:(id)sender {
    
//    NSLog(@"((UISlider *) sender).value == %d",(int)((UISlider *) sender).value);
    
    if ([self.popoverKey isEqualToString:@"BRAODCASTING_POWER"]) {
        
        self.slider.value = ((UISlider *) sender).value ;
        [self setBroadcastingValue:(int)((UISlider *) sender).value];
        
    } else if ([self.popoverKey isEqualToString:@"ADVERTISING_INTERVAL"]) {
        
        [self setAdvertisingIntervalValue:((UISlider *) sender).value];

    
    }
}

- (void)setAdvertisingIntervalValue:(float) value {
    
    unsigned short currentValue = (-1) * value + 2000.0;
   
    self.firstFieldValueLabel.text = [NSString stringWithFormat:@"%hu ms",currentValue];
    


}
-(void)setBroadcastingValue:(int) value {
    
    if (value == 4) {
        
        self.firstFieldValueLabel.text = @"Strong (4 dbm)";
        self.secondFieldValueLabel.text = @"~ 70m";
        
        
    } else if (value == 0) {
        
        self.firstFieldValueLabel.text = @"Strong (0 dbm)";
        self.secondFieldValueLabel.text = @"~ 50m";
        

    } else if (value == -4) {
        
        self.firstFieldValueLabel.text = @"Normal (-4 dbm)";
        self.secondFieldValueLabel.text = @"~ 40m";
        
        

    } else if (value == -8) {
        
        self.firstFieldValueLabel.text = @"Normal (-8 dbm)";
        self.secondFieldValueLabel.text = @"~ 30m";
        

    } else if (value == -12) {
        
        self.firstFieldValueLabel.text = @"Weak (-12 dbm)";
        self.secondFieldValueLabel.text = @"~ 15m";
        

    } else if (value == -16) {
        
        self.firstFieldValueLabel.text = @"Weak (-16 dbm)";
        self.secondFieldValueLabel.text = @"~ 7m";
        

    } else if (value == -20) {
        
        self.firstFieldValueLabel.text = @"Weak (-20 dbm)";
        self.secondFieldValueLabel.text = @"~ 3.5m";
        

    } else if (value == -30) {
        
        self.firstFieldValueLabel.text = @"Weak (-30 dbm)";
        self.secondFieldValueLabel.text = @"~ 1.5m";
        


    }


}

- (void) setPowerValue:(ESTBeaconPower)value {
    
    
    NSLog(@"is app connected to estimote == %d", [self.beacon isConnected]);
    [self.beacon writeBeaconPower:value withCompletion:^(ESTBeaconPower value, NSError *error) {
        
        if (error) {
            
            NSLog(@"power setting error == %@",error);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"IDChangedFailedNotification" object:nil];
            
        } else {
            
            NSMutableDictionary * notifDict = [NSMutableDictionary dictionary];
            
            [notifDict setObject:[NSNumber numberWithInt:value] forKey:@"newPowerValue"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"IDChangedSucessNotification" object:notifDict];
            
        }
    }];


}
- (IBAction)sliderTouchUpInsideEvent:(id)sender {

    if ([self.popoverKey isEqualToString:@"BRAODCASTING_POWER"]) {
        
        float currentPowerValue = [[[[self.firstFieldValueLabel.text componentsSeparatedByString:@" "] objectAtIndex:1] substringFromIndex:1] floatValue];
        
        // set power value
        NSLog(@"current power value %0f",currentPowerValue);
        [self setPowerValue:currentPowerValue];
        
    } else if ([self.popoverKey isEqualToString:@"ADVERTISING_INTERVAL"]) {
    
    unsigned short currentValue =[[[self.firstFieldValueLabel.text componentsSeparatedByString:@" "] firstObject] integerValue];
    
    [self.beacon writeBeaconAdvInterval:currentValue withCompletion:^(unsigned short value, NSError *error) {
    
        if (error) {
            
            NSLog(@"errorrrrr %@",error);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"IDChangedFailedNotification" object:nil];
        }
        
        NSMutableDictionary * notifDict = [NSMutableDictionary dictionary];
        
        [notifDict setObject:[NSNumber numberWithInt:value] forKey:@"newAdInterval"];

        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"IDChangedSucessNotification" object:notifDict];
        
        
    }];
    }
}
@end
