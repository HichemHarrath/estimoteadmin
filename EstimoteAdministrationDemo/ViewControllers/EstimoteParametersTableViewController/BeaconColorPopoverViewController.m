//
//  BeaconColorPopoverViewController.m
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 18/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import "BeaconColorPopoverViewController.h"

@interface BeaconColorPopoverViewController ()

@property (nonatomic, retain) NSMutableArray * colorsDatasource ;


@end

@implementation BeaconColorPopoverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        self.colorsDatasource = [NSMutableArray arrayWithArray:@[@"Icy Marshmallow",@"Blueberry Pie",@"Coctail Mint"]];
        
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.colorPicher selectRow:0 inComponent:0 animated:YES];
    [self.beaconColor setImage:[UIImage imageNamed:@"estimote_beacon.png"]];
    self.selectedColorImageName = @"estimote_beacon.png";
    
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.colorsDatasource count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self.colorsDatasource objectAtIndex:row];
}


#pragma mark -
#pragma mark PickerView Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    switch (row) {
            
        case 0:{
            
            //Icy Marshmallow
            [self.beaconColor setImage:[UIImage imageNamed:@"estimote_beacon.png"]];
            self.selectedColorImageName = @"estimote_beacon.png";
            
            [self saveBeaconColor:self.selectedColorImageName];
            break;
        }
        case 1:{
            
            //Blueberry Pie
            [self.beaconColor setImage:[UIImage imageNamed:@"BlueberryPieEstimoteIcon.png"]];
            self.selectedColorImageName = @"BlueberryPieEstimoteIcon.png";
            [self saveBeaconColor:self.selectedColorImageName];

            break;
        }
        case 2:{
            
            //Coctail Mint
            [self.beaconColor setImage:[UIImage imageNamed:@"CoctailMintEstimoteIcon.png"]];
            self.selectedColorImageName = @"CoctailMintEstimoteIcon.png";
            [self saveBeaconColor:self.selectedColorImageName];

            break;
        }
        default:
            break;
    }

}

-(void) saveBeaconColor:(NSString *) imageName {
    
    [NSUserDefaults standardUserDefaults] ;
    
    
   NSString *  imageNameKey = [NSString stringWithFormat:@"%@.%ld.%ld", [self.beacon.proximityUUID UUIDString], (long)[self.beacon.major integerValue], (long)[self.beacon.minor integerValue]];
    [[NSUserDefaults standardUserDefaults] setObject:imageName forKey:imageNameKey];
                
   
    
}


@end
