//
//  EstimotePrametersTableViewController.h
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 10/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESTBeacon.h"

@interface EstimotePrametersTableViewController : UITableViewController <UIPopoverControllerDelegate>


@property (strong, nonatomic) IBOutlet UILabel *UUIDLabel;
@property (strong, nonatomic) IBOutlet UILabel *majorLabel;
@property (strong, nonatomic) IBOutlet UILabel *minorLabel;
@property (strong, nonatomic) IBOutlet UILabel *colorLabel;

@property (strong, nonatomic) IBOutlet UILabel *hardwareRevisionLabel;
@property (strong, nonatomic) IBOutlet UILabel *operatingSystemLabel;
@property (strong, nonatomic) IBOutlet UILabel *broadcastingPowerLabel;

@property (strong, nonatomic) IBOutlet UILabel *advertisingIntervalLabel;
@property (strong, nonatomic) IBOutlet UILabel *batteryLevelLabel;



@property (nonatomic, retain) ESTBeacon * beacon;


@end
