//
//  PubCollectionViewController.h
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 15/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PubCollectionViewController : UICollectionViewController

@property(nonatomic, retain) NSMutableArray * pubArray;

@end
