//
//  PopoverContentViewController.h
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 10/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESTBeacon.h"

@interface PopoverContentViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UILabel *toolBarTitle;

- (IBAction)changeBtnPressed:(id)sender;
- (IBAction)saveBtnPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *changeBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *saveBtn;


@property (nonatomic, retain) NSString *toolBartitleText;
@property (nonatomic, retain) NSString * helpTextViewText;
@property (nonatomic, retain) NSString *iDLabelText;


@property (strong, nonatomic) IBOutlet UITextView *helpTextView;

@property (strong, nonatomic) IBOutlet UITextField *uUIDLabel;


@property(nonatomic, retain) ESTBeacon * beacon;

@property(nonatomic, retain) NSString * popoverKey ;


@end
