//
//  PubCollectionViewController.m
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 15/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import "PubCollectionViewController.h"

@interface PubCollectionViewController ()

@end

@implementation PubCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.pubArray = [[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"1",@"2",@"3",@"4",@"1",@"2",@"3",@"4"]];
    [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark -
#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    
    if (([self.pubArray count] - section * 2) / 2 != 0)
    {
        return 2;
    }
    else
    {
        return ([self.pubArray count] - section * 2) % 2;
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return  ([self.pubArray count] % 2 == 0 ? [self.pubArray count] / 2 : ([self.pubArray count] / 2) + 1);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    // EHLNewsCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"EHLNewsCell" forIndexPath:indexPath];
    NSString *identifier = @"pubCollectionViewCell";
    
    UICollectionViewCell * cell = [cv dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    return cell;
}



#pragma mark -
#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
//    EHLPresentations * selectedPresentationObject = [self.presentationsArray objectAtIndex:indexPath.section * 3 + indexPath.row];
//    NSLog(@"title selected == %@",[ selectedPresentationObject presentationTitle]);
//    PresentationArticlesViewController * presentationArticlesViewcontroller = [[PresentationArticlesViewController alloc] initWithNibName:@"PresentationArticlesViewController" bundle:nil];
//    presentationArticlesViewcontroller.articlesArray = (NSArray *)[EHLPresentationArticles getAllArticlesWherePresentationID:selectedPresentationObject.presentationID andIsDeleted:@"0"];
//    [self.navigationController pushViewController:presentationArticlesViewcontroller animated:YES];
    
}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return  CGSizeMake(160, 284);
//}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    
    //(CGFloat top, CGFloat left, CGFloat bottom, CGFloat right)
    return UIEdgeInsetsMake(10, 30, 10, 30);
}
@end
