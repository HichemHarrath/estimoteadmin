//
//  PopoverContentViewController.m
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 10/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import "PopoverContentViewController.h"

@interface PopoverContentViewController ()

@end

@implementation PopoverContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.toolBarTitle.text = self.toolBartitleText ;
    self.helpTextView.text = self.helpTextViewText ;
    self.uUIDLabel.text = self.iDLabelText ;
    
    self.changeBtn.enabled = YES;
    self.saveBtn.enabled = NO;
    
    self.uUIDLabel.enabled = NO ;
    self.uUIDLabel.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    
    self.saveBtn.enabled = YES;
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)changeBtnPressed:(id)sender {
    
    self.uUIDLabel.enabled = YES;
    self.changeBtn.enabled = NO;
}

- (IBAction)saveBtnPressed:(id)sender {
    
    self.saveBtn.enabled = NO;
    [self.uUIDLabel resignFirstResponder];
    
    self.changeBtn.enabled = YES;
    self.uUIDLabel.enabled = NO;
    
    
    
    if ([self.popoverKey isEqualToString: @"UIID_POPOVER"]) {
        
        [self.beacon writeBeaconProximityUUID:self.uUIDLabel.text withCompletion:^(NSString *value, NSError *error) {
            self.uUIDLabel.text = [self.beacon.proximityUUID UUIDString];
            
            if (error) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"IDChangedSucessNotification" object:nil];
                
            } else {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"IDChangedFailedNotification" object:nil];

            }
        }];
        
        
        
    } else if ([self.popoverKey isEqualToString: @"MAJOR_POPOVER"]) {
        
        [self.beacon writeBeaconMajor:[self.uUIDLabel.text integerValue]  withCompletion:^(unsigned short value, NSError *error) {
            self.uUIDLabel.text = [NSString stringWithFormat:@"%ld",(long)[self.beacon.major integerValue]];
            
            if (error) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"IDChangedSucessNotification" object:nil];
                
            } else {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"IDChangedFailedNotification" object:nil];
                
            }

        }];
        
        
    
    } else if ([self.popoverKey isEqualToString:@"MINOR_POPOVER"]) {
        
        [self.beacon writeBeaconMinor:[self.uUIDLabel.text integerValue] withCompletion:^(unsigned short value, NSError *error) {
            self.uUIDLabel.text = [NSString stringWithFormat:@"%ld",(long)[self.beacon.minor integerValue]];
            
            if (error) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"IDChangedSucessNotification" object:nil];
                
            } else {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"IDChangedFailedNotification" object:nil];
                
            }

        }];
    
    }
    
    
}
@end
