//
//  EstimotePrametersTableViewController.m
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 10/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import "EstimotePrametersTableViewController.h"

#import "PopoverContentViewController.h"
#import "CustomPopoverContentViewController.h"
#import "BeaconColorPopoverViewController.h"

#import "ESTBeacon.h"

@interface EstimotePrametersTableViewController ()

@property (nonatomic, strong) UIPopoverController * popOver;
@property (nonatomic, strong) UIPopoverController * customPopover;


@property (nonatomic, strong) UIPopoverController * colorPickerPopover;
@property (nonatomic, retain) BeaconColorPopoverViewController * beaconColorPopoverViewController ;


@property (nonatomic, retain) PopoverContentViewController * popoverContentViewController ;
@property (nonatomic, retain) CustomPopoverContentViewController * customPopoverContentViewController ;

@property (nonatomic, retain) NSString * beaconImageFileName;
@end

@implementation EstimotePrametersTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    if (section == 0) {
        
        return 3 ;
        
    } else if (section == 1) {
        
        return 2;
        
    }
    
    return 4;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.section == 0) {
        
        switch (indexPath.row) {
                
            case 0:{
                // UUID static cell
                [self showPopoverWithKey:@"UIID_POPOVER" forCell:[tableView cellForRowAtIndexPath:indexPath]] ;
                break;
            }
                
            case 1:{
                // UUID static cell
                [self showPopoverWithKey:@"MAJOR_POPOVER" forCell:[tableView cellForRowAtIndexPath:indexPath]] ;
                break;
            }
                
            case 2:{
                // UUID static cell
                [self showPopoverWithKey:@"MINOR_POPOVER" forCell:[tableView cellForRowAtIndexPath:indexPath]] ;
                break;
            }

            default:
                break;
        }
    } else if (indexPath.section == 1) {
        
        if (indexPath.row == 0) {
            
            [self presentPopoverWithKey:@"BRAODCASTING_POWER" forCell:[tableView cellForRowAtIndexPath:indexPath]];
            
        } else if (indexPath.row == 1){
            
            [self presentPopoverWithKey:@"ADVERTISING_INTERVAL" forCell:[tableView cellForRowAtIndexPath:indexPath]];

        
        }
    
    
    } else if (indexPath.section == 2) {
    
        if (indexPath.row == 1) {
            [self presentColorsPickerPopoverforCell:[tableView cellForRowAtIndexPath:indexPath]];
        }
    }

}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) showPopoverWithKey:(NSString *) popoverKey  forCell:(UITableViewCell *) cell{
    
    
    
    
    if (!self.popoverContentViewController) {
        UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.popoverContentViewController = (PopoverContentViewController *) [storyBoard instantiateViewControllerWithIdentifier:@"popoverContentViewController"];
        
        self.popoverContentViewController.beacon = self.beacon;
        

    }

    if ([popoverKey isEqualToString:@"UIID_POPOVER"]) {
        
        self.popoverContentViewController.popoverKey = @"UIID_POPOVER" ;
        
        self.popoverContentViewController.toolBartitleText = @"UUID";
        self.popoverContentViewController.iDLabelText = cell.detailTextLabel.text ;
        self.popoverContentViewController.helpTextViewText =@"Note that default UUID for all Estimote beacons is B9407F30-F5F8-466E-AFF9-25556B57FE6D."
        @"\n All your beacons must have the same UUID"
        @"\n Please keep UUID's value to be able to see your beacons using this app.";
        
    } else if ([popoverKey isEqualToString:@"MAJOR_POPOVER"]){
        
        self.popoverContentViewController.popoverKey = @"MAJOR_POPOVER" ;

        
        self.popoverContentViewController.toolBartitleText = @"Major";
        self.popoverContentViewController.iDLabelText = cell.detailTextLabel.text ;  //@"B9407F30-F5F8-466E-AFF9-25556B57FE6D";
        
        self.popoverContentViewController.helpTextViewText = @"Here you can change the major field of this beacon"
        @"  \n \n  Note that major id is unique for a set of beacons";

    } else if ([popoverKey isEqualToString:@"MINOR_POPOVER"]) {
        
        self.popoverContentViewController.popoverKey = @"MINOR_POPOVER" ;

        
        self.popoverContentViewController.toolBartitleText = @"Minor";
        self.popoverContentViewController.iDLabelText = cell.detailTextLabel.text ;  //@"B9407F30-F5F8-466E-AFF9-25556B57FE6D";
        self.popoverContentViewController.helpTextViewText = @"Here you can change the minor id of this beacon"
        @"\n Note that minor id must be unique for this Estimote beacon"
        @"\n This value identifies a beacon between beacons that have the same major id.";

    
    
    }
    
    if (!self.popOver) {
        
        self.popOver = [[UIPopoverController alloc] initWithContentViewController:self.popoverContentViewController];
        self.popOver.delegate = self ;
    }
    
    [self.popOver presentPopoverFromRect:cell.frame
                                  inView:self.tableView
                permittedArrowDirections:UIPopoverArrowDirectionUp
                                animated:YES];

}

- (void) presentPopoverWithKey:(NSString *) popoverKeyStr  forCell:(UITableViewCell *) cell{
    
    
    if (!self.customPopoverContentViewController) {
        
        UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        self.customPopoverContentViewController = (CustomPopoverContentViewController *) [storyBoard instantiateViewControllerWithIdentifier:@"CustomPopoverContentViewController"];
        
        self.customPopoverContentViewController.beacon = self.beacon;
        
        
    }
    
    if ([popoverKeyStr isEqualToString:@"BRAODCASTING_POWER"]) {
        
        self.customPopoverContentViewController.popoverKey = @"BRAODCASTING_POWER" ;
        
    } else if ([popoverKeyStr isEqualToString:@"ADVERTISING_INTERVAL"]){
        
        self.customPopoverContentViewController.popoverKey = @"ADVERTISING_INTERVAL" ;
        
    }
    
    if (!self.customPopover) {
        
        self.customPopover = [[UIPopoverController alloc] initWithContentViewController:self.customPopoverContentViewController];
        self.customPopover.delegate = self ;
    }
    
    [self.customPopover presentPopoverFromRect:cell.frame
                                  inView:self.tableView
                permittedArrowDirections:UIPopoverArrowDirectionUp
                                animated:YES];


}

-(void) popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    
    if (popoverController == self.popOver) {
        
        self.popOver = nil;
        self.popoverContentViewController = nil;

    } else if (popoverController == self.customPopover){
        
        self.customPopover = nil;
        self.customPopoverContentViewController = nil;

    
    } else if (popoverController == self.colorPickerPopover) {
        
        self.beaconImageFileName = self.beaconColorPopoverViewController.selectedColorImageName ;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDidChangeColorNotification" object:self.beaconImageFileName];
        
        self.colorPickerPopover = nil;
        self.beaconColorPopoverViewController = nil;

    
    }

}

-(void)presentColorsPickerPopoverforCell:(UITableViewCell *) cell {
    
    if (!self.beaconColorPopoverViewController) {
        
        UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.beaconColorPopoverViewController = (BeaconColorPopoverViewController *) [storyBoard instantiateViewControllerWithIdentifier:@"ColorPicker"];
        self.beaconColorPopoverViewController.beacon = self.beacon ;
    }
    
    if (!self.colorPickerPopover) {
        
        self.colorPickerPopover = [[UIPopoverController alloc] initWithContentViewController:self.beaconColorPopoverViewController];
        
        self.colorPickerPopover.delegate = self ;
    }
    
    [self.colorPickerPopover presentPopoverFromRect:cell.frame
                                        inView:self.tableView
                      permittedArrowDirections:UIPopoverArrowDirectionUp
                                      animated:YES];



}

@end
