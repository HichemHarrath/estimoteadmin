//
//  LogInViewController.h
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 15/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "BeaconHTTPClient.h"

@interface LogInViewController : UIViewController <BeaconHTTPClientDelegate>
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;

- (IBAction)loginBtnPressed:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;

@property (strong, nonatomic) IBOutlet UILabel *loginErrorLabel;

@end
