//
//  LogInViewController.m
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 15/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import "LogInViewController.h"

@interface LogInViewController ()

@end

@implementation LogInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.usernameTextField.text = @"";
    self.passwordTextField.text = @"";
    [self.usernameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)loginBtnPressed:(id)sender {
    
    if ([[self.usernameTextField.text uppercaseString] isEqualToString:@"ADMIN"] && [[self.passwordTextField.text uppercaseString] isEqualToString:@"ADMIN"] ) {
        
        NSDictionary * parameters = nil;
//        NSString * loginURLStr = @"" ;
        
        BeaconHTTPClient * client = [BeaconHTTPClient sharedBeaconHTTPClient];
        client.delegate = self;
        
        [client loginWithParameters:parameters];
        
        
        [self performSegueWithIdentifier:@"loginSucess" sender:sender];
        
    } else {
        
        self.loginErrorLabel.text = @"Please chech your username and password. admin, admin for demo.";
        
    
    }
}

-(void) beaconHTTPClient:(BeaconHTTPClient *)client didLoginWithSuccess:(id) response {
    //login sucess
    
}

-(void) beaconHTTPClient:(BeaconHTTPClient *)client didFailLoginWithError:(NSError *) error {
    // login failure

}





@end
