//
//  CustomPopoverContentViewController.h
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 11/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ESTBeacon.h"

@interface CustomPopoverContentViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *toolBarTitle;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *changeBtn;
@property (strong, nonatomic) IBOutlet UILabel *sliderMaximumLabel;
- (IBAction)sliderAction:(id)sender;

@property (strong, nonatomic) IBOutlet UISlider *slider;
@property (strong, nonatomic) IBOutlet UILabel *sliderMinimumLabel;
- (IBAction)changeBtnPressed:(id)sender;


@property (strong, nonatomic) IBOutlet UITextView *helpTextView;



@property (nonatomic, retain) ESTBeacon *beacon;


@property (strong, nonatomic) IBOutlet UILabel *firstFieldTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *secondFieldTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *firstFieldValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *secondFieldValueLabel;


- (IBAction)sliderTouchUpInsideEvent:(id)sender;


@property (nonatomic, retain) NSString * popoverKey;

@property(nonatomic, assign) float sliderCurrentValue;
@property(nonatomic, assign) float sliderMinValue;
@property(nonatomic, assign) float sliderMaxValue;

@end
