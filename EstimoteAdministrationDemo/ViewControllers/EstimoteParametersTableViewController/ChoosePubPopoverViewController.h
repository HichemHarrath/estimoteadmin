//
//  ChoosePubPopoverViewController.h
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 18/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChoosePubPopoverViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *beaconWebView;

@end
