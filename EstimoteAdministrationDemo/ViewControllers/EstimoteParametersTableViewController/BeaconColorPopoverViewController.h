//
//  BeaconColorPopoverViewController.h
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 18/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESTBeacon.h"

@interface BeaconColorPopoverViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) IBOutlet UIPickerView *colorPicher;
@property (strong, nonatomic) IBOutlet UIImageView *beaconColor;

@property (nonatomic, retain) NSString * selectedColorImageName ;

@property (nonatomic, retain) ESTBeacon * beacon;



@end
