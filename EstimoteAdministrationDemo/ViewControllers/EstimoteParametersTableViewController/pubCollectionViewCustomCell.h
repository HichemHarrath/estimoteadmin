//
//  pubCollectionViewCustomCell.h
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 15/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pubCollectionViewCustomCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *pubTitleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *pubImageView;
@property (strong, nonatomic) IBOutlet UILabel *pubDescriptionLabel;
@property (strong, nonatomic) IBOutlet UIButton *pubReadMoreBtn;

@end
