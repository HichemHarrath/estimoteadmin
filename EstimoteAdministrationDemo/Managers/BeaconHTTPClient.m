//
//  BeaconHTTPClient.m
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 18/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import "BeaconHTTPClient.h"
#import "AFHTTPRequestOperationManager.h"

 static NSString * const BeaconHTTPClientBaseURL = @"REMOVED FOR CONFIDENTIALITY" ;

@implementation BeaconHTTPClient


+ (BeaconHTTPClient *) sharedBeaconHTTPClient{
    
    static BeaconHTTPClient * _sharedBeaconHTTPClient = nil;
    
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedBeaconHTTPClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:BeaconHTTPClientBaseURL]];
    });
    
    return _sharedBeaconHTTPClient ;

}

- (instancetype) initWithBaseURL:(NSURL *) url {
    
    self = [super initWithBaseURL:url];
    
    if (self) {
        
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    
    return self;
    
}

// login method
- (void) loginWithParameters:(NSDictionary *) parameters {

    
    [self GET:@"" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        // success
        
        if ([self.delegate respondsToSelector:@selector(beaconHTTPClient:didLoginWithSuccess:)]) {
            [self.delegate beaconHTTPClient:self didLoginWithSuccess:responseObject];
        }
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //failure
        
        if ([self.delegate respondsToSelector:@selector(beaconHTTPClient:didFailLoginWithError:)]) {
            [self.delegate beaconHTTPClient:self didFailLoginWithError:error];
        }
    }];

}

-(void) saveBeaconWithParameters:(NSDictionary *)parameters {

    
    [self GET:@"services/add.php" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        // success
        
        if ([self.delegate respondsToSelector:@selector(beaconHTTPClient:didSaveBeaconWithSuccess:)]) {
            [self.delegate beaconHTTPClient:self didSaveBeaconWithSuccess:responseObject];
        }
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //failure
        
        if ([self.delegate respondsToSelector:@selector(beaconHTTPClient:didFailSavingBeaconWithError:)]) {
            [self.delegate beaconHTTPClient:self didFailSavingBeaconWithError:error];
        }
    }];

}


@end
