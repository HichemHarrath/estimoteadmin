//
//  BeaconHTTPClient.h
//  EstimoteAdministrationDemo
//
//  Created by Hichem iMac on 18/04/2014.
//  Copyright (c) 2014 Hichem HARRATH. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@protocol BeaconHTTPClientDelegate ;

@interface BeaconHTTPClient : AFHTTPSessionManager


@property (nonatomic, weak) id <BeaconHTTPClientDelegate> delegate;

+(BeaconHTTPClient *) sharedBeaconHTTPClient;
- (instancetype) initWithBaseURL:(NSURL *)url;


// login method
- (void) loginWithParameters:(NSDictionary *) parameters ;

//save beacon
- (void) saveBeaconWithParameters:(NSDictionary *) parameters ;

@end

@protocol BeaconHTTPClientDelegate   <NSObject>

@optional

// login methods
-(void) beaconHTTPClient:(BeaconHTTPClient *)client didLoginWithSuccess:(id)response ;
-(void) beaconHTTPClient:(BeaconHTTPClient *)client didFailLoginWithError:(NSError *)error ;


// save beacon  methods
-(void) beaconHTTPClient:(BeaconHTTPClient *)client didSaveBeaconWithSuccess:(id)response ;
-(void) beaconHTTPClient:(BeaconHTTPClient *)client didFailSavingBeaconWithError:(NSError *)error ;


//Save beacon methods

@end

